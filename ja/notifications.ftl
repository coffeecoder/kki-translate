NEW_ADMIN_TITLE = あなたは管理者です
NEW_ADMIN_MESSAGE = { $oldAdmin }は大会を退出しました。あなたが管理者に昇格されました。エリミネーション開始や結果編集の権限が与えられました。
NEW_MATCH_TILE = 新規試合
NEW_MATCH_MESSAGE = { $teammate }と{ $tableName }テーブルで試合です！
# Table 1, 2 'and' 3
AND = 及び

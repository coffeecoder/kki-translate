NEW_ADMIN_TITLE = Sen Admin'sin
NEW_ADMIN_MESSAGE = { $oldAdmin } turnuvadan ayrıldı. Şans seni yeni yönetici seçti. Bu sana elemeleri başlatmaya ve sonuçları düzenleme imkanı verir.
NEW_MATCH_TILE = Yeni maç
NEW_MATCH_MESSAGE = Şimdi { $teammate } ile { $tableName } masasında oynuyorsun.
# Table 1, 2 'and' 3
AND = ve

NEW_ADMIN_TITLE = Jsi Správce
NEW_ADMIN_MESSAGE = { $oldAdmin } opustil turnaj. Náhodou jsi byl vybrán jako nový správce. To ti dává příležitost zahájit eliminaci a upravovat výsledky.
NEW_MATCH_TILE = Nový Zápas
NEW_MATCH_MESSAGE = Hraješ s { $teammate } na stole { $tableName }!
# Table 1, 2 'and' 3
AND = a
YOU_HAVE_BEEN_KICKED_TITLE = Byl jsi vyhozen
YOU_HAVE_BEEN_KICKED_TEXT = Admin tě odebral z turnaje

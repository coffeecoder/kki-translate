NEW_ADMIN_TITLE = Du bist Admin
NEW_ADMIN_MESSAGE = {$oldAdmin} hat das Turnier verlassen. Der Zufall hat dich als neuen Admin gewählt. Damit bekommst du die Möglichkeit das KO zu starten und Ergebnisse zu bearbeiten.
NEW_MATCH_TILE = Neues Match
NEW_MATCH_MESSAGE = Du spielst jetzt mit {$teammate} an Tisch {$tableName}!
# Table 1, 2 'and' 3
AND = und
YOU_HAVE_BEEN_KICKED_TITLE = Du wurdest gekickt
YOU_HAVE_BEEN_KICKED_TEXT = Der Admin hat dich aus dem Turnier entfernt

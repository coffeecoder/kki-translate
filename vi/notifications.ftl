NEW_ADMIN_TITLE = Bạn là quản trị viên
NEW_ADMIN_MESSAGE = { $oldAdmin } đã rời khỏi giải đấu. Thật tình cờ, bạn đã được chọn làm quản trị viên mới. Điều này cho bạn cơ hội để bắt đầu loại bỏ và chỉnh sửa kết quả.
NEW_MATCH_TILE = Trận đấu mới
NEW_MATCH_MESSAGE = Bạn hiện đang chơi với { $teammate } tại bàn { $tableName }!
# Table 1, 2 'and' 3
AND = và

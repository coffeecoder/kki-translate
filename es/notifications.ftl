NEW_ADMIN_TITLE = Eres administrador
NEW_ADMIN_MESSAGE = { $oldAdmin } ha abandonado el torneo. Por casualidad, has sido elegido como nuevo administrador. Esto te da la oportunidad de iniciar la eliminación y editar los resultados.
NEW_MATCH_TILE = Nueva partida
NEW_MATCH_MESSAGE = ¡Ahora estás jugando con{ $teammate }en la mesa { $tableName }!
# Table 1, 2 'and' 3
AND = y

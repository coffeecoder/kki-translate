NEW_ADMIN_TITLE = You are Admin
NEW_ADMIN_MESSAGE = {$oldAdmin} has left the tournament. By chance, you have been chosen as the new admin. This gives you the opportunity to start the elimination and edit results.
NEW_MATCH_TILE = New Match
NEW_MATCH_MESSAGE = You are now playing with {$teammate} at table {$tableName}!
# Table 1, 2 'and' 3
AND = and
YOU_HAVE_BEEN_KICKED_TITLE = You have been kicked
YOU_HAVE_BEEN_KICKED_TEXT = The admin removed you from the tournament

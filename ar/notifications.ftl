NEW_ADMIN_TITLE = انت مشرف
NEW_ADMIN_MESSAGE = { $oldAdmin } ترك البطولة. اختارك الصدفة لتصبح المشرف. وبالتالي، يمكنك الحصول على إمكانية بدء النهائيات وتعديل النتائج.
NEW_MATCH_TILE = مباراة جديدة
NEW_MATCH_MESSAGE = انت الآن تلعب مع { $teammate } على الطاولة { $tableName }
# Table 1, 2 'and' 3
AND = و

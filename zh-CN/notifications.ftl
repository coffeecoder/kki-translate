NEW_ADMIN_TITLE = 你是管理者
NEW_ADMIN_MESSAGE = { $oldAdmin } 已經離開賽事。因此你被指派成為管理者，這將給你開始淘汰賽與編輯結果的權利。
NEW_MATCH_TILE = 新賽事
NEW_MATCH_MESSAGE = 你正在跟 { $teammate }在第{ $tableName }桌對戰!
# Table 1, 2 'and' 3
AND = 以及

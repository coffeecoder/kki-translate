NEW_ADMIN_TITLE = 你是管理員
NEW_ADMIN_MESSAGE = { $oldAdmin } 已經離開此賽事。因此你將成為新的管理員，這將給你能進入淘汰賽以及編輯成績的權利。
NEW_MATCH_TILE = 新場次
NEW_MATCH_MESSAGE = 你正在跟 { $teammate } 在第 { $tableName }桌進行比賽!
# Table 1, 2 'and' 3
AND = 以及

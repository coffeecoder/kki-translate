NEW_ADMIN_TITLE = Tu sei Admin
NEW_ADMIN_MESSAGE = { $oldAdmin } ha lasciato il torneo. Per caso, sei stato scelto come nuovo amministratore. Questo ti dà l'opportunità di iniziare l'eliminazione e modificare i risultati.
NEW_MATCH_TILE = Nuovo Incontro
NEW_MATCH_MESSAGE = Stai giocando con { $teammate } al tavolo { $tableName }!
# Table 1, 2 'and' 3
AND = e
YOU_HAVE_BEEN_KICKED_TITLE = Sei stato kickato
YOU_HAVE_BEEN_KICKED_TEXT = L'admin ti ha rimosso dal torneo
